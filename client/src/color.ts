export interface Color {
    r: number,
    b: number,
    g: number,
    a: number | undefined
}

function isColor(i: any): i is Color {
    if (typeof i === "object" && i.r === "number" && typeof i.g === "number" && typeof i.b === "number") {
        if (i.a) {
            return typeof i.a === "number"
        } else {
            return true
        }
    } else {
        return false
    }
}

