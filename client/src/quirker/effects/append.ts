import { Effect, StackEffect, StackNode } from "..";

function append(node: StackNode): StackEffect {
    return nodes => {
        let neoNodes = [node];
        neoNodes.push(...nodes);
        return neoNodes;
    }
}

const Append: Effect = {
    name: "append",
    constructor: vec => {
        let arg = vec.pop();
        if (arg !== undefined) {
            if (typeof arg === 'string' || typeof arg === 'object') {
                vec.push(append(arg));
            }
            else {
                return "Argument cannot be an effect."

            }
        } else {
            return "Not enough arguments for append.";
        }
    }
}

export default Append;