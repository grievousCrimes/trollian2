import { StackEffect } from "../";

export function stringReplacer(map: (text: string) => string): StackEffect {
    return nodes => {
        for (let i = 0; i < nodes.length; i++) {
            let node = nodes[i];
            if (typeof node === 'string') {
                node = map(node);
            } else {
                switch (node.kind) {
                    case "styled":
                    case "spoiler":
                        let res = stringReplacer(map)(node.children);
                        if (typeof res === "string") {
                            return res;
                        } else {
                            node.children = res;
                        }
                        break;
                    case "quote":
                    case "code":
                    case "image":
                        break;
                }
            }
            nodes[i] = node;
        }
        return nodes;
    }
}
