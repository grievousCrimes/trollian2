import libra from './assets/libra.svg'
import './app.css'
// import { Component } from 'preact'

export function App() {
  return (
    <>
      <img src={libra} class="libra" />
      <div class="claim">
        <a href="/quirk-test">T3ST H3R3</a>
      </div>
    </>
  )
}
