use std::time::Duration;

use axum::{extract::Path, routing::get, Router};
use serde::Deserialize;
use sqlx::{postgres::PgPoolOptions, Postgres, Pool};

#[derive(Deserialize)]
struct Params {
    path: String,
}
#[cfg(not(debug_assertions))]
const PATH: &'static str = "0.0.0.0:8002";

#[cfg(debug_assertions)]
const PATH: &'static str = "0.0.0.0:3002";

fn db_address() -> String {
    #[cfg(not(debug_assertions))]
    return format!(
        "postgres://{}:{}@localhost/trollian2",
        std::env::var("POSTGRESQL_USER").unwrap_or("postgres".into()),
        std::env::var("POSTGRESQL_PASSWORD").expect("You must set POSTGRESQL_PASSWORD.")
    );

    #[cfg(debug_assertions)]
    return String::from("postgres://postgres:password@localhost/trollian2");
}

async fn setup_database() -> Pool<Postgres> {
    let db_connection_addr = db_address();
    let pool = PgPoolOptions::new()
        .max_connections(5)
        .acquire_timeout(Duration::from_secs(3))
        .connect(&db_connection_addr)
        .await
        .expect("Could not connect to database.");

    sqlx::migrate!("./migrations")
        .run(&pool)
        .await
        .unwrap();

    pool
}

#[tokio::main]
async fn main() {
    let app = Router::new().route("/*path", get(any));
    axum::Server::bind(&PATH.parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap();
}

async fn any(Path(Params { path }): Path<Params>) -> String {
    path
}
